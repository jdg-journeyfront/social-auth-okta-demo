import social_django.strategy
from social_core.utils import setting_name

from . import models


class DatabaseSettingStrategy(social_django.strategy.DjangoStrategy):
    def get_backend(self, name, redirect_uri=None, *args, **kwargs):
        try:
            social_backend = models.SocialAuthBackend.objects.get(name=name)
        except models.SocialAuthBackend.DoesNotExist:
            return super().get_backend(name, redirect_uri, *args, **kwargs)
        else:
            Backend = social_backend.backend_class
            backend = Backend(self, redirect_uri=redirect_uri, *args, **kwargs)
            backend.setting_source = social_backend
            return backend

    def setting(self, name, default=None, backend=None):
        """Fetch setting from SocialAuthBackend if available or fallback to Django Settings."""
        get_setting = self.get_setting
        names = [setting_name(name), name]
        if backend:
            names = [setting_name(backend.name, name)] + names
            if hasattr(backend, "setting_source"):

                def get_setting(n: str):
                    return backend.setting_source.setting(name) or self.get_setting(n)

        for name in names:
            try:
                return get_setting(name)
            except (AttributeError, KeyError):
                pass
        return default
