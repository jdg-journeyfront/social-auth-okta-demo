import encrypted_fields.fields
from django.db import models
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _
from social_core.utils import module_member
from social_django.models import (USER_MODEL, AbstractUserSocialAuth,
                                  DjangoStorage)


class CustomUserSocialAuth(AbstractUserSocialAuth):
    user = models.ForeignKey(
        USER_MODEL,
        related_name="custom_social_auth",
        on_delete=models.CASCADE,
    )


class CustomDjangoStorage(DjangoStorage):
    user = CustomUserSocialAuth


BACKEND_CLASSES = [
    ("social_core.backends.okta_openidconnect.OktaOpenIdConnect", "Okta OIDC"),
]


class SocialAuthBackend(models.Model):
    """Similar to adding an AUTHENTICATION_BACKEND to the settings.

    Choosing a particular backend class, register that class with a unique slug and bind
    it to an Account. The BackendSetting table is a key/value store of settings for each
    backend.
    """

    social_auth_backend_id = models.BigAutoField(primary_key=True)
    name = models.SlugField(
        _("Name (key)"),
        unique=True,
        max_length=255,
    )
    cls = models.CharField(
        _("Class"),
        max_length=255,
        help_text=_("Backend implementation class"),
        choices=BACKEND_CLASSES,
    )
    account = models.ForeignKey(
        "Account",
        models.CASCADE,
    )

    @property
    def backend_class(self):
        return module_member(self.cls)

    def setting(self, key) -> str | None:
        try:
            return self.settings.get(key=key).value
        except BackendSetting.DoesNotExist:
            return None

    def __str__(self):
        return f"{self.name} - {self.get_cls_display()}"


class BackendSetting(models.Model):
    """Settings for each SocialAuthBackend."""

    backend_setting_id = models.BigAutoField(
        primary_key=True,
    )
    backend = models.ForeignKey(
        "SocialAuthBackend",
        models.CASCADE,
        related_name="settings",
    )
    key = models.CharField(
        "Key",
        max_length=255,
        help_text=_("CAPITAL_CASE"),
    )
    _value_clear = models.TextField(
        "Value (cleartext)",
        null=True,
        blank=True,
        help_text=_("Unencrypted plain text value"),
    )
    _value_crypt = encrypted_fields.fields.EncryptedTextField(
        "Value (encrypted)",
        null=True,
        blank=True,
        help_text=_("Encrypted secure text value"),
    )

    @cached_property
    def value(self) -> str | None:
        return self._value_clear or self._value_crypt

    class Meta:
        unique_together = [
            ("backend", "key"),
        ]


class Account(models.Model):
    account_id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name
