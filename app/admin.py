from django.contrib import admin

from . import models

# Register your models here.


@admin.register(models.Account)
class AccountAdmin(admin.ModelAdmin):
    pass


class BackendSettingInline(admin.TabularInline):
    model = models.BackendSetting


@admin.register(models.SocialAuthBackend)
class SocialAuthBackendAdmin(admin.ModelAdmin):
    inlines = [BackendSettingInline]
